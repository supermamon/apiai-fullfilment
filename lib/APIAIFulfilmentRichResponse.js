const appName = 'apiai:Response'

class APIAIFulfilmentRichResponse {
  constructor (source) {
    this.source = source || 'anonymous'
    this.speech = null
    this.displayText = null
    this.messages = []
  }

  get () {
    return {
      source: this.source,
      speech: this.speech,
      displayText: this.displayText,
      messages: this.messages
    }
  }

  static buildResponse () {
    return new APIAIFulfilmentRichResponse()
  }

  static addSimpleResponse (message) {
    // web
    this.messages.push({
      type: 0,
      speech: message
    })

    // google
    this.messages.push({
      type: 'simple_response',
      platform: 'google',
      textToSpeech: message
    })

    // telegram
    this.messages.push({
      type: 0,
      platform: 'telegram',
      speech: message
    })

    return this
  }

  static addListCard (title, items) {
    // web
    this.messages.push({
      type: 0,
      speech: title
    })

    // google
    this.messages.push({
      type: 'list_card',
      platform: 'google',
      title: title,
      items: items
    })

    // telegram
    this.messages.push({
      type: 4,
      platform: 'telegram',
      payload: {
        telegram: {
          text: title,
          reply_markup: {
            inline_keyboard: items
          }
        }
      }
    })

    return this
  }

  static addListItem (caption, value) {

  }
}

module.exports = APIAIFulfilmentRichResponse
