const MODULE_ID = 'apiai:Router'
const logger    = require('../utils/logger')
const request   = require('request-promise-native')

class APIAIFulfilmentRouter {
    constructor (opts) {
        this.opts = opts
        this.fulfillers = []

        logger.debug('[%s] new instance created.', MODULE_ID)
    } // constructor

    forward (req, res) {
        var self = this
        return new Promise((resolve, reject) => {
            var matchedIndex = -1
            for (var i = 0; i < self.fulfillers.length; i++) {
                // debug('running filter for %s', self.fulfillers[i].url)
                if (self.fulfillers[i].filter(req)) {
                    // debug('match found')
                    matchedIndex = i
                    break
                } else {
                    // debug('not matched')
                }
            }

            if (matchedIndex !== -1) {
                logger.info('[%s] forwarding to service', MODULE_ID, self.fulfillers[matchedIndex].url)

                var headers = self.fulfillers[matchedIndex].headers || {}
                var options = {
                    method: 'POST',
                    uri: self.fulfillers[matchedIndex].url,
                    headers: headers,
                    body: req.body,
                    json: true // Automatically stringifies the body to JSON
                }
                request(options)
                    .then((data) => {
                        logger.info('[%s] response received from service.', MODULE_ID)
                        resolve(data)
                    })
                    .catch((err) => {
                        logger.error('[%s] error encountered from service.', MODULE_ID, err)
                        reject(err)
                    })
            } else {
                reject(new Error('There is no configured handler for this request'))
            }
        })
    }

    route (obj) {
        // TODO: Validate
        this.fulfillers.push(obj)
    }
}

module.exports = APIAIFulfilmentRouter
