const MODULE_ID = 'apiai:TextResponse'
const logger    = require('../utils/logger')

module.exports = (text, source) => {
    return {
        source: source || MODULE_ID,
        speech: text,
        displayText: text
    }
}

logger.debug('[%s] module loaded', MODULE_ID)
