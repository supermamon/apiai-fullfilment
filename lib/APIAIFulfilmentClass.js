
const MODULE_ID = 'apiai:Fulfilment'

class APIAIFulfilmentClass {
    constructor (opts) {
        this.opts = opts
        this.responders = new Map()
    } // constructor

    addResponder (actionName, responderFn) {
        this.responders.set(actionName, responderFn)
    }

    handleQuery (request, response) {
        this.request  = request
        this.response = response
        this.body = this.request.body
        var self = this
        var action = this.body.result.action

        var Response = require('./APIAIFulfilmentResponse')

        if (!this.responders.get(action)) {
            return self.response.json(new Response('I am not trained to do that yet.'))
        }

        Promise.resolve(this.responders.get(action)(this))
            .then((data) => { self.response.json(data) })
            .catch((err) => { self.response.json(new Response(err.message)) }) // catch
    } // handleQuery
} // class

module.exports = APIAIFulfilmentClass
