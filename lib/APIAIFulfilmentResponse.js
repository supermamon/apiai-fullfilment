const appName = 'apiai:Response'

class APIAIFulfilmentResponse {
    constructor (reply, messages, source) {
        this.source = source || 'noisy-boy'
        this.speech = reply
        this.displayText = reply
        this.messages = messages
    }
}

module.exports = APIAIFulfilmentResponse
