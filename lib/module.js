const MODULE_ID = 'apiai:fulfillment'
const logger    = require('../utils/logger')

module.exports = {
    Fulfillment: require('./APIAIFulfilmentClass'),
    Fulfilment: require('./APIAIFulfilmentClass'),
    Response: require('./APIAIFulfilmentResponse'),
    Router: require('./APIAIFulfilmentRouter'),
    RichResponse: require('./APIAIFulfilmentRichResponse'),
    TextResponse: require('./TextResponse')
}

logger.debug('[%s] module loaded', MODULE_ID)
