const winston   = require('winston')
const logger    = new (winston.Logger)({
    level: process.env.DEBUG_LEVEL || 'info',
    transports: [
        new (winston.transports.Console)({
            silent: false,
            timestamp: true
        })
    ],
    exitOnError: false
})

module.exports = logger
logger.debug('[logger] initialized.')
logger.info('[logger] ENV DEBUG_LEVEL =', process.env.DEBUG_LEVEL||'info')

